import * as TYPES from '../types';

export function replace(route) {
  return {
    type: TYPES.ROUTER_REPLACE,
    payload: route
  }
}