import * as TYPES from '../types';

export function showToast(options) {
  return {
    type: TYPES.SHOW_TOAST,
    payload: options
  }
}