import * as TYPES from '../types';

export function fetchAuthStart() {
  return {
    type: TYPES.FETCHING_AUTH_START
  }
}

export function fetchAuthEnd() {
  return {
    type: TYPES.FETCHING_AUTH_END
  }
}

export function signUp(credentials) {
  return {
    type: TYPES.SIGN_UP,
    payload: credentials
  }
}

export function login(credentials) {
  return {
    type: TYPES.LOG_IN,
    payload: credentials
  }
}

export function loginSocial(social) {
  return {
    type: TYPES.SIGN_UP_SOCIAL,
    payload: social
  }
}

export function smsLogin(phone) {
  return {
    type: TYPES.PASSWORDLESS_LOGIN,
    payload: phone
  }
}

export function smsVerify(credentials) {
  return {
    type: TYPES.PASSWORDLESS_VERIFY,
    payload: credentials
  }
}

export function getApiAccessToken(authToken) {
  return {
    type: TYPES.GET_API_TOKEN,
    payload: authToken
  }
}

export function parseHash() {
  return {
    type: TYPES.PARSE_HASH
  }
}

export function logout() {
  return {
    type: TYPES.LOG_OUT
  }
}

export function setUser(user) {
  return {
    type: TYPES.SET_USER,
    payload: user
  }
}

export function setApiToken(token) {
  return {
    type: TYPES.SET_API_TOKEN,
    payload: token
  }
}

export function syncUser(req) {
  return {
    type: TYPES.SYNC_USER,
    payload: req
  }
}

export function updateUser(formData) {
  return {
    type: TYPES.UPDATE_USER,
    payload: formData
  }
}