export const SHOW_TOAST = 'SHOW_TOAST';
//auth
export const SET_USER = 'SET_USER';
export const UPDATE_USER = 'UPDATE_USER';
export const SYNC_USER = 'SYNC_USER';
export const FETCHING_AUTH_START = 'FETCHING_AUTH_START';
export const FETCHING_AUTH_END = 'FETCHING_AUTH_END';
export const SIGN_UP = 'SIGN_UP';
export const SIGN_UP_SOCIAL = 'SIGN_UP_SOCIAL';
export const LOG_IN = 'LOG_IN';
export const RESET_PASSWORD = 'RESET_PASSWORD';
export const PARSE_HASH = 'PARSE_HASH';
export const LOG_OUT = 'LOG_OUT';
export const ROUTER_REPLACE = 'ROUTER_REPLACE';
export const PASSWORDLESS_LOGIN = 'PASSWORDLESS_LOGIN';
export const PASSWORDLESS_VERIFY = 'PASSWORDLESS_VERIFY';
export const GET_API_TOKEN = 'GET_API_TOKEN';
export const SET_API_TOKEN = 'SET_API_TOKEN';
//profile
export const PATCH_PROFILE = 'PATCH_PROFILE';