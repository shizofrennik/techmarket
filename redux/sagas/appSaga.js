import { takeEvery } from 'redux-saga/effects';
import { toast } from 'react-toastify';
import * as TYPES from '../types';

function* showToast(action) {
  const options = action.payload;
  toast[options.type](options.message);
}

export default [
  takeEvery(TYPES.SHOW_TOAST, showToast)
];
