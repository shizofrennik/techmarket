import {all, call, put, take, takeLatest, select} from 'redux-saga/effects'
import Router from 'next/router';
import AuthService from '../../services/AuthService';
import StorageService from '../../services/StorageService';
import { showToast } from '../actions/app';
import { setUser, fetchAuthStart, fetchAuthEnd, setApiToken, getApiAccessToken } from '../actions/auth';
import { fetchApiToken, fetchUpdateUser, fetchUser } from '../../services/api'
import { replace } from '../actions/navigation';
import * as TYPES from '../types';
import jwtDecode from 'jwt-decode';
import axios from 'axios';
// import Router from 'next/router'

function* signUpFlow(action) {
  try {
    const credentials = action.payload;
    yield put(fetchAuthStart());
    yield call(AuthService.register, credentials);
    yield call(AuthService.login, credentials);
  } catch(error) {
    yield put(showToast({message: error.description, type: "error"}));
  } finally {
    yield put(fetchAuthEnd());
  }
}

function* loginFlow(action) {
  try {
    yield put(fetchAuthStart());
    yield call(AuthService.login, action.payload);
  } catch (error) {
    yield put(showToast({message: error.description, type: "error"}));
  } finally {
    yield put(fetchAuthEnd());
  }
}

function* socialFlow(action) {
  try {
    yield put(fetchAuthStart());
    yield call(AuthService.social, action.payload);
  } catch (error) {
    yield put(showToast({message: error.description, type: "error"}));
  } finally {
    yield put(fetchAuthEnd());
  }
}

function* passwordlessFlow(action) {
  try {
    yield put(fetchAuthStart());
    yield call(AuthService.passwordless, action.payload);
  } catch (error) {
    yield put(showToast({message: error.description, type: "error"}));
  } finally {
    yield put(fetchAuthEnd());
  }
}

function* passwordlessVerifyFlow(action) {
  try {
    yield put(fetchAuthStart());
    console.log('start verify')
    yield call(AuthService.passwordlessVerify, action.payload);
  } catch (error) {
    console.log('error: ', error)
    yield put(showToast({message: error.description, type: "error"}));
  } finally {
    yield put(fetchAuthEnd());
  }
}

function* parseHash() {
  try {
    const hash = yield call(AuthService.parseHash);
    yield put(setUser(jwtDecode(hash.idToken)));
    yield put(getApiAccessToken(`idToken=${hash.idToken}`));
    yield put(replace('/profile'));
  } catch (error) {
    yield put(showToast({message: 'Something goes wrong!', type: "error"}));
    yield put(replace('/'));
  }
}

function* logoutFlow(action) {
  try {
    yield call(AuthService.logout);
    location.href = '/';
  } catch (error) {
    yield put(showToast({message: error.description, type: "error"}));
  }
}

function* setUserFlow(action) {
  try {
    const {req, isServer} = action.payload;
    const token = yield call(StorageService.getUserData, req);

    if(isServer && token) {
      const authToken = yield call(StorageService.getAuthToken, req);
      yield put(getApiAccessToken(authToken));
      const res = yield call(fetchApiToken, authToken);
    }

    if(!isServer && token && user && token.email != user.email) yield put(setUser(token));
  } catch (error) {
    console.log(error);
  }
}

function* getApiAccessTokenFlow(action) {
  try {
    const apiToken = action.payload
    const authUser = jwtDecode(apiToken.split(';').map(c => c.trim()).find(c => c.startsWith('idToken=')).split('idToken=')[1]);
    const res = yield call(fetchApiToken, apiToken);
    StorageService.setItem('apiToken', res.access_token);
    const combinedUser = authUser ? {...authUser, ...res} : res;
    yield put(setApiToken(res.access_token));
    yield put(setUser(combinedUser));
  } catch (error) {
    console.log('error', error)
  }
}

function* updateUserFlow(action) {
  try {
    const formData = action.payload;
    const apiToken = StorageService.getItem('apiToken');
    const user = yield select((state) => state.auth.user);
    const updatedUser = yield call(fetchUpdateUser, user.id, apiToken, formData);
    yield put(setUser({...user, ...updatedUser}));
  } catch (error) {
    console.log('error', error)
  }
}

export default [
  takeLatest(TYPES.SIGN_UP, signUpFlow),
  takeLatest(TYPES.SIGN_UP_SOCIAL, socialFlow),
  takeLatest(TYPES.LOG_IN, loginFlow),
  takeLatest(TYPES.LOG_OUT, logoutFlow),
  takeLatest(TYPES.PASSWORDLESS_LOGIN, passwordlessFlow),
  takeLatest(TYPES.PASSWORDLESS_VERIFY, passwordlessVerifyFlow),
  takeLatest(TYPES.PARSE_HASH, parseHash),
  takeLatest(TYPES.GET_API_TOKEN, getApiAccessTokenFlow),
  takeLatest(TYPES.SYNC_USER, setUserFlow),
  takeLatest(TYPES.UPDATE_USER, updateUserFlow)
];