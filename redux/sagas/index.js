
import {all} from 'redux-saga/effects'
import authSaga from './authSaga';
import appSaga from './appSaga';
import es6promise from 'es6-promise'
import 'isomorphic-unfetch'

es6promise.polyfill()

function* rootSaga() {
  yield all([
    ...authSaga,
    ...appSaga
  ]);
}

export default rootSaga;
