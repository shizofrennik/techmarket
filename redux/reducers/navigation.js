import * as TYPES from '../types';
import Router from 'next/router';

export default function navigation(state = {}, action) {
  switch (action.type) {
    case TYPES.ROUTER_REPLACE:
      Router.replace(action.payload);
      return state;
    default:
      return state;
  }
}