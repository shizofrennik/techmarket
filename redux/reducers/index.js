import { combineReducers } from 'redux';
import auth from './auth';
import navigation from './navigation';
import { reducer as formReducer } from 'redux-form'

export default combineReducers({ auth, navigation, form: formReducer })