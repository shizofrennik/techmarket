import * as TYPES from '../types';
import Router from 'next/router';

const initialState = {
  user: null,
  apiToken: null,
  auth: false,
  fetchingAuth: false
}

export default function auth(state = initialState, action) {
  switch (action.type) {
    case TYPES.FETCHING_AUTH_START:
      return {
        ...state,
        fetchingAuth: true,
      };
    case TYPES.FETCHING_AUTH_END:
      return {
        ...state,
        fetchingAuth: false
      };
    case TYPES.LOG_OUT:
      return {
        ...state,
        user: null,
        auth: false
      };
    case TYPES.SET_USER:
      const user = action.payload
      console.log('set user: ', {...state, user});
      return {
        ...state,
        user,
        auth: !!user
      };
    case TYPES.SET_API_TOKEN:
      const apiToken = action.payload
      console.log('set api token: ', {...state, apiToken});
      return {
        ...state,
        apiToken
      };
    default:
      return state
  }
}