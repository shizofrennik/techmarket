const config = {
  clientID: 'EK1yJUan3MhTaZMzyFqMZvnUl9Xm1zW8',
  domain: 'ekadesign.auth0.com',
  redirectUri: 'https://tech-market-2018.herokuapp.com/auth',
  audience: 'http://45.55.78.237/api',
  leeway: 60,
  scope: "openid email profile",
  responseType: "id_token token",
}

export default config;
