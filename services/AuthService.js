import StorageService from './StorageService';
import Auth0Service from './Auth0Service';
import ScheduleService from './ScheduleService';

class AuthService {
  constructor(auth0Service, storageService, ScheduleService) {
    this.auth0 = auth0Service;
    this.storage = storageService;
    this.scheduler = new ScheduleService(this.storage, this.auth0);

    this.social = this.social.bind(this);
    this.login = this.login.bind(this);
    this.register = this.register.bind(this);
    this.parseHash = this.parseHash.bind(this);
    this.logout = this.logout.bind(this);
    this.passwordless = this.passwordless.bind(this);
    this.passwordlessVerify = this.passwordlessVerify.bind(this);

    this.scheduler.setRenewal();
  }

  login(credentials) {
    const {username, password} = credentials;
    return this.auth0.login(username, password);
  }

  register(credentials) {
    console.log('auth service register creds: ', credentials);
    const {username, email, password} = credentials;
    console.log(username, email, password);
    return this.auth0.register(username, email, password);
  }

  passwordless(phone) {
    return this.auth0.passwordless(phone);
  }

  social(social) {
    return this.auth0.social(social);
  }

  passwordlessVerify(credentials) {
    const { phoneNumber, verificationCode } = credentials;
    return this.auth0.passwordlessVerify(phoneNumber, verificationCode);
  }

  parseHash() {
    return this.auth0.parseHash().then((hash) => {
      this.storage.setAuthData(hash);
      this.scheduler.setRenewal();
      return hash;
    });
  }

  logout() {
    return this.auth0.logout().then(() => {
      this.scheduler.resetRenewal();
      this.storage.unsetAuthData();
    });
  }
};

export default new AuthService(Auth0Service, StorageService, ScheduleService);