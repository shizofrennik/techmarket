import Cookie from 'js-cookie';
import jwtDecode from 'jwt-decode';

class StorageService {
  constructor(storage) {
    this.storage = Cookie;

    this.getItem = this.getItem.bind(this);
    this.setItem = this.setItem.bind(this);
    this.setAuthData = this.setAuthData.bind(this);
    this.unsetAuthData = this.unsetAuthData.bind(this);
    this.getUserData = this.getUserData.bind(this);
  }

  getItem(key) {
    if (!process.browser) return

    return this.storage.get(key);
  }

  setItem(key, value) {
    if (!process.browser) return

    this.storage.set(key, value)
  }

  setAuthData(hash) {
    if (!hash || !process.browser) return;
    const {idToken, accessToken, expiresIn} = hash;

    this.storage.set('idToken', idToken);
    this.storage.set('accessToken', accessToken);
    this.storage.set('expiresIn', +new Date() + (expiresIn * 1000));
  }

  unsetAuthData() {
    if (!process.browser) return

    this.storage.remove('idToken');
    this.storage.remove('accessToken');
    this.storage.remove('expiresIn');
  }

  getAuthToken(req = {}) {
    let idToken = null;

    if (!process.browser && req.headers && req.headers.cookie) {
      idToken = req.headers.cookie.split(';').map(c => c.trim()).find(c => c.startsWith('idToken='));
    } else {
      idToken = this.getItem('idToken');
    }

    return idToken ? idToken : null;
  }

  getUserData(req = {}) {
    let idToken = null;

    if (!process.browser && req.headers && req.headers.cookie) {
      idToken = req.headers.cookie.split(';').map(c => c.trim()).find(c => c.startsWith('idToken='));
    } else {
      idToken = this.getItem('idToken');
    }

    return idToken ? jwtDecode(idToken) : null;
  }
};

export default new StorageService(Cookie);