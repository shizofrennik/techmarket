import auth0 from 'auth0-js';
import config from '../auth0.config';

class Auth0Service {
  constructor(realm = "Username-Password-Authentication") {
    this.realm = realm;

    this.auth0 = new auth0.WebAuth(config);
    this.renewToken = this.renewToken.bind(this);
  }

  social(social) {
    return new Promise((resolve, reject) => {
      this.auth0.authorize({ connection: social }, (err, res) => (err) ? reject(err) : resolve(res));
    });
  }

  login(username, password) {
    return new Promise((resolve, reject) => {
      const options = { username, password, realm: this.realm };

      this.auth0.login(options, (err, res) => (err) ? reject(err) : resolve(res));
    })
  }

  register(username, email, password) {
    console.log('auth0: ', username, email, password);
    return new Promise((resolve, reject) => {
      const options = { email, username, password, connection: this.realm };

      this.auth0.signup(options, (err, res) => (err) ? reject(err) : resolve(res));
    });
  }

  passwordless(phone_number) {
    return new Promise((resolve, reject) => {
      const options = { phone_number, connection: 'sms', send: 'code' };
      this.auth0.passwordlessStart(options, (err, res) => (err) ? reject(err) : resolve(res));
    });
  }

  passwordlessVerify(phoneNumber, verificationCode) {
    return new Promise((resolve, reject) => {
      const options = { phoneNumber, verificationCode, connection: 'sms'};
      console.log('optinos: ', options);
      this.auth0.passwordlessVerify(options, (err, res) => (err) ? reject(err) : resolve(res));
    });
  }

  renewToken() {
    return new Promise((resolve, reject) => {
      this.auth0.checkSession({}, (err, res) => (err) ? reject(err) : resolve(res));
    });
  }

  parseHash() {
    return new Promise((resolve, reject) => {
      this.auth0.parseHash((err, res) => (err) ? reject(err) : resolve(res));
    });
  }

  logout() {
    return Promise.resolve(this.auth0.logout({returnTo: `${window.location.protocol}//${window.location.host}`}));
  }
};

export default new Auth0Service();