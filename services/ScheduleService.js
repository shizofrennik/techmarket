class ScheduleService {

  constructor(storageService, auth0Service) {
    this.timerId = null;

    this.storage = storageService;
    this.auth0 = auth0Service;

    this.setRenewal = this.setRenewal.bind(this);
    this.resetRenewal = this.resetRenewal.bind(this);
    this._renewToken = this._renewToken.bind(this);
  }

  setRenewal() {
    let expiresIn = this.storage.getItem('expiresIn');
    if(!expiresIn) return;

    const delay = expiresIn - Date.now();

    if (delay > 0) {
      this.resetRenewal();
      this.timerId = setTimeout(this._renewToken, delay)
    } else {
      this._renewToken();
    }
  }

  resetRenewal() {
    if(this.timerId) clearTimeout(this.timerId);
  }

  _renewToken() {
    return this.auth0.renewToken().then(this.storage.setAuthData).then(this.setRenewal);
  }
};

export default ScheduleService;