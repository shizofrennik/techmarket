import axios from 'axios';

const baseUrl = 'http://45.55.78.237/api/v1/'

export function fetchLogin(auth0UserUrl, data) {
  return axios.post("https://" + auth0UserUrl + "/oauth/token", data).then(resp => resp.data)
}

export function fetchSignUp(auth0UserUrl, data) {
  return axios.post("https://" + auth0UserUrl + "/dbconnections/signup", data, {validateStatus: (status) => {
      return status < 500;
  }})
}

export function fetchApiToken(apiToken) {
  return axios.post(`${baseUrl}Clients/login-client?access_token=${apiToken}`).then(res => res.data);
}

export function fetchUser(userId, apiToken) {
  return axios.get(`${baseUrl}Clients/${userId}?access_token=${apiToken}`).then(res => res.data);
}

export function fetchUpdateUser(userId, apiToken, formData) {
  return axios.patch(`${baseUrl}Clients/${userId}?access_token=${apiToken}`, formData).then(res => res.data);
}
