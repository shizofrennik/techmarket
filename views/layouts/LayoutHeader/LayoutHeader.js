import React, { Component } from 'react';
import {connect} from 'react-redux';
import Logo from './img/logo.png';
import login from './img/login.png';
import User from './img/user.png';
import Search from './img/search.png';
import Avatar from './img/avatar.png';
import arUp from './img/arrow-up.png';
import arDown from './img/arrow-down.png';
import Link from 'next/link';
import RoleSwitcher from '../../components/RoleSwitcher';
import AuthUser from '../AuthUser';

const roleSwitcherStyles = {
  wrapper: {
    display: 'flex',
    border: '1px solid #800',
    fontSize: '18px',
    fontWeight: 'normal'
  },
  item: {
    width: '50%',
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    cursor: 'pointer',
    paddingTop: '2px',
    margin: 0
  },
  activeItem: {
    background: '#880000',
    color: '#fff'
  }
}

class LayoutHeader extends Component {
  constructor () {
    super();
    this.state = {
      showPopup: false,
      showWorkPopup: false,
      showUserPopup: false,
    };
    this.togglePopup = this.togglePopup.bind(this);
    this.toggleWorkPopup = this.toggleWorkPopup.bind(this);
    this.toggleUserPopup = this.toggleUserPopup.bind(this);
 }

  togglePopup () {
    this.setState({showPopup: !this.state.showPopup})
  }

  toggleWorkPopup () {
    this.setState({showWorkPopup: !this.state.showWorkPopup})
  }

  toggleUserPopup () {
    const { user } = this.props;
    if (user) this.setState(({ showUserPopup }) => ({ showUserPopup: !showUserPopup }))
  }

  render() {
    const { user, updateUser, logout } = this.props;
    // @todo remove ternary
    const role = (user && user.role) ? user.role : null;

    return (
      <div>
        <div className="header">
          <div className="container">
            <div className="header__wrapper">
              <div className="header__block">
              <div className="header__block-main">
                <div className="header__ham">
                  <i className="fas fa-bars" onClick={this.togglePopup}/>
                  {this.state.showPopup && <div className="header__popup-menu">
                    <div className="header__popup-wrapper">
                      <div className="header__popup-search">
                        <input type="text" placeholder="найти технику"/>
                        <div className="header__popup-search-img"><img src={Search} alt=""/></div>
                      </div>
                      <a href="">как это работает</a>
                      <a href="">Партнерская программа</a>
                      <a href="">Правовая информация</a>
                      <a href="">Для владельцев техники</a>
                      <a href="">Тарифы</a>
                      <a href="">Вопрос/Ответ</a>
                      <a href="">Контакты</a>
                    </div>
                  </div>}
                </div>
                <div className="header__logo">
                <Link href="/">
                  <a ><img src={Logo} alt="" /></a>
                </Link>
                </div>
                </div>
                <div className="header__menu">
                  <div className="header__menu-item"><a href="#">создать заявку на технику</a></div>
                  <div className="header__menu-item"><a href="#">Каталог техники</a></div>
                  <div className="header__menu-item"><a href="#" onClick={this.toggleWorkPopup}>Работа</a>
                  {this.state.showWorkPopup && <div className="header__menu-popup">
                                                  <div className="header__menu-popup-wrapper">
                                                      <a href="">предложить работу</a>
                                                      <a href="">Хочу выполнить работу</a>
                                                  </div>
                                                </div>}

                  </div>
                  <div className="header__menu-item"><a href="#">для владельцев техники</a></div>

                </div>
              </div>
              <div className="header__auth">
                <Link href={this.props.user ?
                                            '/profile' :
                                            '/sign-in'}>
                  <a  className="header__auth-item" onMouseEnter={this.toggleUserPopup}>
                    {user ?
                          <AuthUser modal={this.state.showUserPopup} user={this.props.user} /> :
                          <img src={login} className="header__auth-img auth__img" alt=""/>}
                    {!this.props.user && <div className="">Вход</div>}
                  </a>
                </Link>

                {/* {this.props.user ? <div className="btn__exit" onClick={logout}>Выйти</div> : ''} */}
                {this.state.showUserPopup && <div className="user-popup">
                  <div className="user-popup__wrapper">
                    {role && <RoleSwitcher role={role} onChange={updateUser} styles={roleSwitcherStyles} />}
                    {this.props.user.role==="customer" && <div className="user-popup__menu">
                      <a href="">Баланс: 6516 р.  <span className="user-popup__add">пополнить</span></a>
                      <a href="">Избранное     (1)</a>
                      <a href="">Мои заявки на технику     (1)</a>
                      <a href="">Мои заявки о работе     (1)</a>
                      <a href="">Финансовые операции</a>
                      <a href="">Заработок в системе</a>
                      <a href="">Отзывы     (2/1)</a>
                      <Link href="/profile"><a href="">Мой профиль</a></Link>
                      <a href="">Настройки</a>
                      <span className="user-popup__out" onClick={logout}>Выйти</span>
                    </div>}

                     {this.props.user.role==="perfomer" && <div className="user-popup__menu">
                      <a href="">Баланс: 6516 р.  <span className="user-popup__add">пополнить</span></a>
                      <a href="">Тариф: Стандарт  <span className="user-popup__add"> изменить</span></a>
                      <a href="">Моя техника    (1)</a>
                      <a href="">Отклики на заявки     (+1)</a>
                      <a href="">Финансовые операции</a>
                      <a href="">Заработок в системе</a>
                      <a href="">Отзывы     (2/1)</a>
                      <Link href="/profile"><a href="">Мой профиль</a></Link>
                      <a href="">Настройки</a>
                      <span className="user-popup__out" onClick={logout}>Выйти</span>
                    </div>}

                  </div>
                </div>}
              </div>
            </div>
          </div>
        </div>


      <style jsx>{`



        .header {
          background-image: url("/static/img/header-bg.jpg");
          border-bottom: 1px solid  #fff;
        }
        .container {
          width: 1368px;
          max-width: 1368px;
          padding-right: 0;
          padding-left: 0;
        }
        .header__wrapper {
          display: flex;
          justify-content: space-between;
          height: 65px;
        }
        .header__block {
          display: flex;
          justify-content: space-between;
          align-items: center;
          margin-right: 20px;
        }
        .header__block-main {
          display: flex;
          align-items: center;
          margin-right: 30px;
        }
        .header__ham {
          margin-right: 25px;
          color: #fff;
          font-size: 22px;
          position: relative;
          cursor: pointer;
        }
        .header__menu {
          display: flex;
          justify-content: space-around;
          align-items: center;
          font-weight: lighter;
        }
        .header__menu-item {
          text-transform: uppercase;
          font-size: 18px;
          margin-right: 25px;
          position: relative;
        }
        .header__menu-item:last-child {
          margin-right: 0;
        }
        .header__menu-item a:hover {
          color: #ddd;
        }
        .header__menu-item a {
          color: #fff;
          text-decoration: none;
        }
        .header__auth {
          display: flex;
          align-items: center;
          position: relative;
        }
        .header__auth-item {
          font-size: 16px;
          font-weight: lighter;
          color: #fff;
          display: flex;
          text-decoration: none;
          display: flex;
          align-items: center;

        }
        .header__auth-item:last-child {
          margin-right: 0;
        }

        .header__auth-item:hover .header__auth-img {
          opacity: 0.8;
        }
        .auth__img {
          margin-right: 7px;
          position: relative;
          bottom: 3px;
        }
        .auth__avatar {
          margin-right: 7px;
          height: 50px;
          width: 50px;

        }
        {/* .header__auth-item img:hover {
          opacity: 0.9;
        } */}
        .header__auth-item span {
          margin-right: 15px;
        }
        .btn__exit {
          font-size: 14px;
          line-height: 24px;
        }
        .btn__exit:hover {
          color: #ddd;
        }


      .header__popup-menu {
        position: absolute;
        top: 60px;
        z-index: 7;
        background: #f9f9f9;
        -webkit-box-shadow: 0px 5px 5px 0px rgba(0,0,0,0.2);
        -moz-box-shadow: 0px 5px 5px 0px rgba(0,0,0,0.2);
        box-shadow: 0px 5px 5px 0px rgba(0,0,0,0.2);
      }
      .header__popup-wrapper {
        display: flex;
        flex-direction: column;
        width: 440px;
        padding: 20px;

      }
      .header__popup-wrapper a {
        color: #111;
        text-decoration: none;
        text-transform: uppercase;
        font-size: 18px;
        line-height: 36px;
      }
      .header__popup-wrapper a:hover {
        color: #880000;
      }
      .header__popup-search {
        display: flex;
        position: relative;
        margin-bottom: 20px;
      }
      .header__popup-search input {
        width: 100%;
        height: 40px;
        border: 1px solid #909090;
        font-size: 16px;
        font-weight: 100;
        padding: 0 75px 0 10px;
        outline: none;
      }
      .header__popup-search-img {
        background: #880000;
        width: 70px;
        height: 36px;
        position: absolute;
        top: 2px;
        right: 2px;
        display: flex;
        justify-content: center;
        align-items: center;
      }

      .header__menu-popup {
        position: absolute;
        top: 57px;
        background: #dedede;
        width: 300px;
        -webkit-box-shadow: 0px 5px 5px 0px rgba(0,0,0,0.2);
        -moz-box-shadow: 0px 5px 5px 0px rgba(0,0,0,0.2);
        box-shadow: 0px 5px 5px 0px rgba(0,0,0,0.2);
        z-index: 7;
      }
      .header__menu-popup-wrapper {
        display: flex;
        flex-direction: column;
        padding: 10px 20px;
      }
      .header__menu-popup-wrapper a {
        font-size: 18px;
        line-height: 36px;
        color: #111111;
      }
      .header__menu-popup-wrapper a:hover {
        color: #880000;
      }


      .user-popup {
        position: absolute;
        top: 60px;
        right: 0;
        background: #fff;
        padding: 5px;
        font-size: 16px;
        font-weight: 100;
        width: 210px;
        line-height: 28px;
        -webkit-box-shadow: 0px 5px 5px 0px rgba(0,0,0,0.2);
        -moz-box-shadow: 0px 5px 5px 0px rgba(0,0,0,0.2);
        box-shadow: 0px 5px 5px 0px rgba(0,0,0,0.2);
        z-index: 7;
        color: #808080;
      }
      .user-popup__wrapper {
        display: flex;
        flex-direction: column;

      }
      .user-popup__toggle {
        display: flex;
        border: 1px solid #800;
        font-size: 18px;
        font-weight: normal;

      }
      .user-popup__toggle>div {
        width: 50%;
        display: flex;
        justify-content: center;
        align-items: center;
        cursor: pointer;
        padding-top: 2px;
      }
      .user-popup__active {
        background: #880000;
        color: #fff;
      }
      .user-popup__add {
        color: #800;
        text-align: right
      }
      .user-popup__menu {
        padding: 10px 15px;
        display: flex;
        flex-direction: column;

      }
      .user-popup__menu a {
        color: #808080;
        text-decoration: none;
        display: flex;
        justify-content: space-between;
      }
      .user-popup__menu a:hover {
        color: #800;
      }
      .user-popup__out {
        font-size: 18px;
        cursor: pointer;
      }



     @media (max-width: 1440px) {
        .container {
          width: 1200px;
        }
        .header__ham {
          margin-right: 15px;
        }
      }
      @media (max-width: 1280px) {
        .container {
          width: 1088px;
        }
        .header__menu-item {
          font-size: 16px;
        }
        .header__logo img {
          width: 200px;
        }
      }
      @media (max-width: 1150px) {
        .container {
          width: 990px;
        }
        .header__menu-item {
         margin-right: 20px;
         font-size: 15px;
        }
        .header__block-main {
          margin-right: 15px;
        }
        .header__logo img {
          width: 180px;
        }
        .header__popup-wrapper {
          width: 350px;
        }
        .header__popup-wrapper a {
          font-size: 16px;
        }
        .header__menu-popup {
          width: 240px;
        }
        .header__menu-popup-wrapper a {
          font-size: 16px;
        }
      }
      @media (max-width: 1024px) {
        .container {
          width: auto;
          padding: 0 15px;
        }
        .header__menu-item {
         margin-right: 15px;
        }
      }
    `}
    </style>
      </div>
    );
  }
}

export default LayoutHeader;