import React from 'react';
import {connect} from 'react-redux'
import Header from '../LayoutHeader';
import Footer from '../LayoutFooter';
import {bindActionCreators} from 'redux';
import {logout, updateUser} from '../../../redux/actions/auth';


class Layout extends React.Component {
	render() {
    const { user, updateUser, logout } = this.props;

		return (
			<div>
				<Header user={user} updateUser={updateUser} logout={logout} />
				<div className="cont">{this.props.children}</div>
				<Footer />
				<style jsx>{`
					.cont {
						min-height: 40vh;
					}
				`}</style>
				<style global jsx>{`
						.container {
							width: 1368px;
							max-width: 1368px;
							padding-right: 0;
							padding-left: 0;
						}
						@media (max-width: 1440px) {
							.container {
								width: 1200px;
							}
						}
						@media (max-width: 1280px) {
							.container {
								width: 1088px;
							}
						}
						@media (max-width: 1150px) {
							.container {
								width: 990px;
							}
						}
						@media (max-width: 1024px) {
							.container {
								width: auto;
								padding: 0 15px;
							}
						}


						@font-face {
								font-family: 'PFDinTextCondPro';
								src:  url('/static/fonts/PFDinTextCondPro-regular.ttf'),
								url('/static/fonts/PFDinTextCondPro-medium.ttf'),
								url('/static/fonts/PFDinTextCondPro-light.ttf'),
								url('/static/fonts/PFDinTextCondPro-LightItalic.ttf'),
								url('/static/fonts/PFDinTextCondPro-bold.ttf');
						 }
						body {
								font-family: 'PFDinTextCondPro', sans-serif;
								font-size: 16px;
								background: #e8e8e8;
						}
						.btn {
							border: 1px solid #f70000;
							border-radius: 0;
							background: #880000;
							color: #c8c8c8;
							text-transform: uppercase;
							margin-bottom: 20px;
							font-size: 18px;
							padding: 10px;
							line-height: 28px;
							display: flex;
							justify-content: center;
							align-items: center;
						}
						.page-title {
							display: flex;
							justify-content: center;
							padding-top: 50px;
							margin-bottom: 30px;
						}
						.page-title span{
							font-size: 48px;
							text-transform: uppercase;
							font-weight: 300;
							text-align: center;
						}
						.page-title::before {
							content: '';
							display: block;
							height: 3px;
							width: 40px;
							background: #ff0000;
							position: relative;
							top: 34px;
							left: -20px;
						}
						.page-title::after {
							content: '';
							display: block;
							height: 3px;
							width: 40px;
							background: #ff0000;
							position: relative;
							top: 34px;
							right: -20px;
						}
						.breadcrumbs {
            font-size: 16px;
            color: #880000;
            font-weight: 100;
            padding: 20px 0;
          }
          .breadcrumbs a {
            color: #880000;
            text-decoration: none;
          }
          .breadcrumbs span {
            padding-right: 15px;
            margin-right: 10px;
            position: relative;
            display: inline-block;
          }
          .breadcrumbs span:after {
            content: '';
            display: block;
            position: absolute;
            height: 5px;
            width: 5px;
            background: #880000;
            top: 9px;
            right: 0;
          }
          .breadcrumbs span:last-child {
            color: #888888;
          }
          .breadcrumbs span:last-child:after {
            padding-right: 0;
            margin-right: 0;
            display: none;
          }
				 `}</style>
			</div>
		);
	}
}

const mapStateToProps = (state, props) => {
  return ({
		user: state.auth.user,
	})
}
const mapDispatchToProps = (dispatch) => {
  return bindActionCreators({
    logout,
    updateUser
  }, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(Layout);