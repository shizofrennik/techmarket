import React, { Component } from 'react';
import Logo from './img/logo-footer.png';
import apple from './img/apple.png';
import google from './img/google.png';
import email from './img/email.png';
import phone from './img/phone.png';
import clock from './img/clock.png';
import fb from './img/fb.png';
import vk from './img/vk.png';
import inst from './img/inst.png';


class LayoutFooter extends Component {
  render() {
    return (
      <div>
        <div className="footer">
          <div className="container">
            <div className="footer__wrapper">
              <div className="footer__column">
                <div className="footer__logo">
                  <a href=""><img src={Logo} alt=""/></a>
                </div>
                <div className="footer__support">
                  <span>Служба поддержки</span>
                  <div className="footer__support-item">
                    <img src={email} alt=""/>
                    <span>support@Техклуб.рф</span>
                  </div>
                  <div className="footer__support-item">
                  <img src={phone} alt=""/>
                    <span>+7 (9665) 72-72-72</span>
                  </div>
                  <div className="footer__support-item">
                  <img src={clock} alt=""/>
                    <span>Пн - Пт с 8:00 до 20:00, <br/>Сб - Вс с 9,:00 до 18:00</span>
                  </div>
                </div>
                <div className="footer__apps">
                  <span>Используйте мобильное приложение. <br/>Это удобно!</span>
                  <div className="footer__apps-links">
                    <a href=""><img src={apple} alt="" /></a>
                    <a href=""><img src={google} alt="" /></a>
                  </div>
                </div>
              </div>
              <div className="footer__column">
                <span className="footer__column-title">Компания</span>
                <span className="footer__column-item"><a href="">О проекте</a></span>
                <span className="footer__column-item"><a href="">Вакансии</a></span>
                <span className="footer__column-item"><a href="">Партнерская программа</a></span>
                <span className="footer__column-item"><a href="">Правила сервиса</a></span>
                <span className="footer__column-item"><a href="">Частые вопросы</a></span>
                <span className="footer__column-item"><a href="">Для заказчиков</a></span>
                <span className="footer__column-item"><a href="">Для исполнителей</a></span>
                <span className="footer__column-item"><a href="">Контакты</a></span>
              </div>
              <div className="footer__column">
                <span className="footer__column-title">КАТАЛОГ ТЕХНИКИ</span>
                <span className="footer__column-item"><a href="">Бульдозеры</a></span>
                <span className="footer__column-item"><a href="">Буровые установки</a></span>
                <span className="footer__column-item"><a href="">Грейдеры, автогрейдеры</a></span>
                <span className="footer__column-item"><a href="">Тракторы</a></span>
                <span className="footer__column-item"><a href="">Бульдозеры</a></span>
                <span className="footer__column-item"><a href="">Буровые установки</a></span>
                <span className="footer__column-item"><a href="">Грейдеры, Автогрейдеры</a></span>
                <span className="footer__column-item"><a href="">Смотреть весь каталог</a></span>
              </div>
              <div className="footer__column">
                <span className="footer__column-title">ГЕОГРАФИЯ РАБОТЫ</span>
                <span className="footer__column-item"><a href="">Москва</a></span>
                <span className="footer__column-item"><a href="">Санкт-Петербург</a></span>
                <span className="footer__column-item"><a href="">Новосибирск</a></span>
                <span className="footer__column-item"><a href="">Екатеринбург</a></span>
                <span className="footer__column-item"><a href="">Нижний Новгород</a></span>
                <span className="footer__column-item"><a href="">Казань</a></span>
                <span className="footer__column-item"><a href="">Самара</a></span>
                <span className="footer__column-item footer__column-it"><a href="">И еще 123 города РФ</a></span>
              </div>
            </div>
          </div>
          <div className="footer__bottom">
            <div className="container">
              <div className="footer__bottom-wrapper">
                <div className="footer__bottom-copy">
                  <span>Техклуб.рф © 2018. All rights reserved.</span>
                </div>
                <div className="footer__bottom-social">
                  <span>Мы в соц. сетях.</span>
                  <a href="#" className="footer__social"><img src={fb} alt=""/></a>
                  <a href="#" className="footer__social"><img src={vk} alt=""/></a>
                  <a href="#"><img src={inst} alt=""/></a>
                </div>
              </div>
            </div>
          </div>
        </div>
        <style jsx>{`
          .container {
            width: 1368px;
            max-width: 1368px;
            padding-right: 0;
            padding-left: 0;
          }
          .footer {
            background: #333333;
            color: #ffffff;
            padding-top: 20px;
          }
          .footer__wrapper {
            display: flex;
            justify-content: space-between;
          }
          .footer__logo {
            margin-bottom: 15px;
          }
          {/* .footer__support {
            margin-bottom: 10px;
          } */}
          .footer__support>span {
            text-transform: uppercase;
            margin-bottom: 25px;
            display: block;
          }
          .footer__support-item {
            display: flex;
            padding-left: 50px;
            margin-bottom: 15px;
            align-items: flex-start;
            line-height: 30px;
          }
          .footer__support-item img {
            margin-right: 25px;
          }
          .footer__apps>span {
            display: block;
            margin-bottom: 15px;
          }
          .footer__apps-links {
            margin-bottom: 20px;
            display: flex;
            justify-content: space-between;
          }
          .footer__column {
            display: flex;
            flex-direction: column;
            font-weight: 300;
          }
          .footer__column-title {
            margin-bottom: 30px;
            text-transform: uppercase;
            font-size: 18px;
            padding-top: 10px;
          }
          .footer__column-item {
            margin-bottom: 15px;
          }
          .footer__column-item a:hover {
            color: #ddd;
          }
          .footer__column-it {
            font-style: italic;
            font-weight: 100;
          }
          .footer__column a {
            text-decoration: none;
            padding: 6px 0;
            color: #c1c1c1;
            line-height: 26px;
          }
          .footer__bottom {
            background: #000;
          }
          .footer__bottom-wrapper {
            display: flex;
            justify-content: space-between;
            align-items: center;
          }
          .footer__bottom-social {
            display: flex;
            align-items: center;
            padding: 7px 0;
          }
          .footer__bottom-social>span {
            margin-right: 30px;
          }
          .footer__social {
            margin-right: 20px;
          }
          {/* .footer__social-fb {
            background: url('');
          }
          .footer__social-vk {
            background: url('');
          }
          .footer__social-inst {
            background: url('');
            margin-right: 0; */}
          }






     @media (max-width: 1440px) {
        .container {
          width: 1200px;
        }
      }
      @media (max-width: 1280px) {
        .container {
          width: 1088px;
        }
      }
      @media (max-width: 1150px) {
        .container {
          width: 990px;
        }

      }
      @media (max-width: 1024px) {
        .container {
          width: auto;
          padding: 0 15px;
        }
        .footer__apps-links img {
          width: 110px;
        }
        .footer__logo img {
          width: 240px;
        }
        .footer__support-item {
          padding-left: 10px;
        }
        .footer__column {
          min-width: 180px;
        }


        `}
        </style>
      </div>
    );
  }
}

export default LayoutFooter;