import React, { Component } from 'react'
import {connect} from 'react-redux';
import Avatar from './img/avatar.png';
import arUp from './img/arrow-up.png';
import arDown from './img/arrow-down.png';
import Link from 'next/link';

export default class AuthUser extends Component {
  render() {
    const { user, updateUser, logout } = this.props;
    return (
      <div>
        <div className="authlink">
          <div className="authlink-img">
            <img src={user.avatar_link ? user.avatar_link : Avatar}/>
          </div>
          <div className="authlink-name">{this.props.user.name}
            <img className="authlink-arr" src={this.props.modal ? arUp : arDown} />
          </div>
          <div className="authlink-message"><span>55</span></div>
        </div>
        <style jsx>{`
          img {
            width: 30px;
          }
          .authlink {
            display: flex;
            align-items: center;
          }
          .authlink-img {
            background: #fff;
            width: 35px;
            border-top: 1px solid #eaeaea;
            border-bottom: 1px solid #eaeaea;
            height: 30px;
            display: flex;
            justify-content: center;
            align-items: center;
          }
          .authlink-name {
            background: #fff;
            width: 140px;
            height: 30px;
            border: 1px solid #eaeaea;
            margin-right: 8px;
            color: #111;
            position: relative;
            padding-left:10px;
            line-height: 30px;
          }
          .authlink-message {
            background: url("/static/img/message.png") no-repeat 0 0;
            width: 30px;
            height: 20px;
          }
          .authlink-message span {
            display: block;
            background: #800;
            color: #fff;
            border-radius: 15px;
            position: relative;
            text-align: center;
            line-height: 20px;
            top: -10px;
            right: -12px;
            width: 20px;
            height: 17px;
          }
          .authlink-arr {
            width: 12px;
            position: absolute;
            top: 10px;
            right: 10px;
          }

      `}</style>
    </div>
    )
  }
}
