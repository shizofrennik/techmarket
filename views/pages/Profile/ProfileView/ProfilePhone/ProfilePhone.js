import React, { Component } from 'react';
import { Field, reduxForm } from 'redux-form';
import Edit from '../img/input-edit.png';
import Date from '../img/input-date.png';
import Phone from '../img/input-phone.png';
import { validation, phoneMask } from '../../../../../helpers/utils';

class ProfilePhone extends Component {
  state = {
    active: false
  }

  edit = () => {
    this.setState({ active: true });
  }

  save = async values => {
    const { onSubmit } = this.props;

    try {
      await onSubmit(values);
      this.setState({ active: false });
    } catch(error) {
      console.log(error);
    }
  }

  render() {
    const { edit, save } = this;
    const { handleSubmit, invalid, submitting } = this.props;
    const { active, value } = this.state;

    return (
      <form className="profile__main-input-item" onSubmit={handleSubmit(save)}>
        <img src={Phone} alt="" className="profile__input-img"/>
        <div className="profile__input-block">
          <span className="profile__input-descr">Тел:</span>
          <Field
            className="profile__input-editable"
            component="input"
            name="phone"
            disabled={!active}
            validate={validation.phone}
            type="tel"
            {...phoneMask}
          />
        </div>
        {active ? (
          <button type="submit" disabled={invalid || submitting}><img src={Date} alt="" /></button>
        ) : (
          <div onClick={edit}><img src={Edit} alt="" /></div>
        )}
      </form>
    )
  }
}

const ProfilePhoneForm = reduxForm({
  form: 'profilePhone',
  enableReinitialize: true
})(ProfilePhone)

export default ProfilePhoneForm;