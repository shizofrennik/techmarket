import React, { Component } from 'react';
import Edit from '../img/input-edit.png';

const acceptedFormats = [
  'image/jpeg',
  'image/png'
];

class AvatarField extends Component {
  state = {
    src: this.props.src
  }

  componentWillReceiveProps(props) {
    const { src } = props;
    if(src !== this.state.src) this.setState({src});
  }

  onChange = (e) => {
    const { onChange } = this.props;
    const file = e.target.files[0];
    const reader = new FileReader();
    reader.readAsDataURL(file);

    reader.onload = () => {
      console.log(reader.result);
      onChange({ avatar_link: reader.result })
      this.setState({ src: reader.result });
    };

    reader.onerror = (error) => {
      console.log(error);
    };
  }

  render() {
    const { onChange } = this;
    const { src } = this.state;

    return (
      <div className="profile__img">
        <label className="profile__img-edit" htmlFor="avatar"><img src={Edit} alt="Edit" /></label>
        <img src={src} alt="Avatar" />
        <input id="avatar" style={{display: 'none'}}
          type="file"
          onChange={onChange}
          accept={acceptedFormats}
        />
        <style jsx>{`
          .profile__img {
            width: 200px;
            height: 200px;
            margin-right: 30px;
            position: relative;
          }
          .profile__img img {
            width: 100%;
          }
          .profile__img-edit {
            opacity: 0;
            position: absolute;
            top: 10px;
            right: 10px;
            cursor: pointer;
            transition: opacity .3s ease;
          }
          .profile__img:hover .profile__img-edit {
            opacity: 1;
          }

          @media (max-width: 1440px) {
              .profile__img {
             width: 150px;
            }
            }
        `}</style>
      </div>
    )
  }
}

export default AvatarField;