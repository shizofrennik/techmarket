import React, { Component } from 'react';
import { Field, reduxForm } from 'redux-form';
import Edit from '../img/input-edit.png';
import Date from '../img/input-date.png';
import Place from '../img/input-place.png';
import { validation, cities } from '../../../../../helpers/utils';
import { SelectInput } from '../../../../components/form-fields';

class ProfileCity extends Component {
  state = {
    active: false
  }

  edit = () => {
    setTimeout(() => {
      this.setState({ active: true });
    }, 0);
  }

  save = async values => {
    const { onSave } = this.props;

    try {
      await onSave(values);
      this.setState({ active: false });
    } catch(error) {
      console.log(error);
    }
  }

  render() {
    const { edit, save } = this;
    const { handleSubmit, invalid, submitting } = this.props;
    const { active } = this.state;

    return (
      <form className="profile__main-input-item" onSubmit={handleSubmit(save)}>
        <img src={Place} alt="" className="profile__input-img"/>
        <div className="profile__input-block">
          <span className="profile__input-descr">Город:</span>
          <Field
            className="profile__input-editable"
            component={SelectInput}
            name="address"
            disabled={!active}
            validate={validation.required}
            placeholder="Выберите город..."
            options={cities}
          />
        </div>
        {active ? (
          <button type="submit" disabled={invalid || submitting}><img src={Date} alt="" /></button>
        ) : (
          <button type="button" onClick={edit}><img src={Edit} alt="" /></button>
        )}
      </form>
    )
  }
}

const ProfileCityForm = reduxForm({
  form: 'profileCity',
  enableReinitialize: true
})(ProfileCity)

export default ProfileCityForm;