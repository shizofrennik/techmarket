import React, { Component } from 'react';
import Edit from '../img/input-edit.png';
import Date from '../img/input-date.png';

class ProfileField extends Component {
  state = {
    value: this.props.value || '',
    active: false
  }

  componentWillReceiveProps(props) {
    const { value } = props;
    if(value !== this.state.value) this.setState({value});
  }

  onChange = (e) => {
    const value = e.target.value;
    this.setState({ value });
  }

  edit = () => {
    this.setState({ active: true });
  }

  save = async () => {
    const { onSave, name } = this.props;
    const { value } = this.state;
    console.log({name, value});

    try {
      await onSave({[name]: value});
      this.setState({ active: false, error: '' });
    } catch(error) {
      console.log(error);
      this.setState({ error });
    }
  }

  render() {
    const { onChange, edit, save } = this;
    const { icon, label, name } = this.props;
    const { active, value, error } = this.state;

    return (
      <div className="profile__main-input-item">
        <img src={icon} alt="" className="profile__input-img"/>
        <div className="profile__input-block">
          {label &&
            <span className="profile__input-descr">{label}</span>
          }
          <input
            type="text"
            className="profile__input-editable"
            name={name}
            onChange={onChange}
            value={value}
            disabled={!active}
          />
        </div>
        {active ? (
          <img src={Date} alt="" onClick={save} />
        ) : (
          <img src={Edit} alt="" onClick={edit} />
        )}
        <style jsx>{`
          .profile__main-input-item {
            font-size: 18px;
            padding: 10px;
            border-bottom: 1px solid #d8d8d8;
            width: 350px;
            display: flex;
            justify-content: flex-end;
            align-items: center;
            position: relative;
          }
          .profile__input-img {
            position: absolute;
            left: 7px;
            top: 13px;
          }
          .profile__input-block {
            margin-right: 10px;
          }
          .profile__input-descr {
            margin-right: 10px;
            font-size: 16px;
            font-weight: lighter;
          }
          .profile__input-editable {
            font-size: 18px;
            text-transform: uppercase;
            padding: 6px 10px 2px;
            border: 1px solid #d8d8d8;
          }
          .profile__input-editable:disabled {
            border: 1px solid #fff;
          }
        `}</style>
      </div>
    )
  }
}

export default ProfileField;