import React, { Component } from 'react'

class StatusField extends Component {
  state = {
    status: this.props.status
  }

  onChange = async (e) => {
    const { onChange } = this.props;
    const value = e.target.value;

    try {
      await onChange({ role: value });
      this.setState({ status: value });
    } catch(error) {
      console.log(error);
    }
  }

  render() {
    const { onChange } = this;
    const { status } = this.state;

    return (
      <div className="profile-status">
        <label className="profile-status-item">
          <input
            checked={status === 'customer'}
            type="radio"
            name="status"
            value="customer"
            className="btn btn-radio"
            onChange={onChange}
          />
          Заказчик
        </label>
        <label className="profile-status-item">
          <input
            checked={status === 'perfomer'}
            type="radio"
            name="status"
            value="perfomer"
            className="btn btn-radio"
            onChange={onChange}
          />
          Испонитель
        </label>
        <style jsx>{`
          .profile-status {
            display: flex;
            padding: 15px 0;
            align-items: center;
          }
          .profile-status-item {
            display: flex;
            margin: 0 20px 0 0;
          }
          .profile-status-item input {
            margin: 0 7px 0 0;
          }
        `}</style>
      </div>
    )
  }
}

export default StatusField;