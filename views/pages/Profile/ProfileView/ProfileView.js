import React, { Component } from 'react';
import { connect } from "react-redux";
import {bindActionCreators} from 'redux';
import User from './img/input-user.png';
import Edit from './img/input-edit.png';
import Date from './img/input-date.png';
import Avatar from './img/avatar.png';
import EditField from './EditField';
import ProfileField from './ProfileField';
import ProfilePhone from './ProfilePhone';
import ProfileEmail from './ProfileEmail';
import ProfileCity from './ProfileCity';
import ProfileDateField from './ProfileDateField';
import AvatarField from './AvatarField';
import StatusField from './StatusField';
import ReactModal from 'react-modal';
import RoleSwitcher from '../../../components/RoleSwitcher';
import moment from 'moment';
import './styles.css';

const roleSwitcherStyles = {
  wrapper: {
    display: 'flex',
    padding: '15px 0',
    alignItems: 'center'
  },
  item: {
    display: 'flex',
    margin: '0 20px 0 0',
    color: '#ccc',
    cursor: 'pointer'
  },
  activeItem: {
    color: '#000'
  }
}

export default class ProfileView extends Component {
  state = {
    promoCode: '',
    promoVisible: false,
  }

  toggleModal = () => this.setState(({ promoVisible }) => ({ promoVisible: !promoVisible }));

  componentDidMount() {
    const {user} = this.props;
    // if (user && user.registered_at && moment().diff(user.registered_at, 'seconds') <= 50 && !user.refered_by) {
    //   this.setState({promoVisible: true});
    // }
    if (user.isCreated) {
      this.setState({promoVisible: true});
    }
  }

  componentWillReceiveProps(newProps) {
    const {user} = this.props;
    if (newProps.user !== user && newProps.user.isCreated) {
      this.setState({promoVisible: true});
    }
  }

  promoChange = (e) => {
    this.setState({ promoCode: e.target.value })
  }

  confirmPromo = () => {
    const { updateUser } = this.props;
    const { promoCode } = this.state;

    updateUser({ 'refered_by': promoCode });
    this.toggleModal();
  }

  render() {
    const {
      toggleModal,
      promoChange,
      confirmPromo,
      handleDateChange,
      toggleStatusModal
    } = this;
    const {
      updateUser,
      logout,
      user: {
        name,
        address,
        email,
        phone,
        updated_at,
        picture,
        avatar_link,
        role,
        birth_date,
        refered_by,
        registered_at
      }
    } = this.props;
    const {
      promoVisible,
      startDate,
      isStatusModalOpen
    } = this.state;

    return (

      <div>
        <div className="container">
          <div className="breadcrumbs">
            <span><a href="">Главная</a></span>
            <span><a href="">Личный кабинет {this.props.user.role==="customer" ? 'Заказчика' : 'Исполнителя'}</a></span>
            <span>Мои профиль</span>
          </div>
          <div className="profile__wrapper">
          {this.props.user.role==="customer" && <div className="profile__menu">
              <ul className="profile__menu-list">
                <li className="profile__menu-item">
                  <a href="">Диалоги (+1)</a>
                </li>
                <li className="profile__menu-item">
                  <a href="">Избранное (1)</a>
                </li>
                <li className="profile__menu-item">
                  <a href="">Мои заявки на технику (1) </a>
                </li>
                <li className="profile__menu-item">
                  <a href="">Мои заявки на работу (1)</a>
                </li>
                <li className="profile__menu-item">
                  <a href="">Финансовые операции</a>
                </li>
                <li className="profile__menu-item">
                  <a href="">Заработок в системе</a>
                </li>
                <li className="profile__menu-item">
                  <a href="">Отзывы (2/1)</a>
                </li>
                <li className="profile__menu-item">
                  <a href="" className="active">Мой профиль</a>
                </li>
                <li className="profile__menu-item">
                  <a href="">Настройки</a>
                </li>
              </ul>
              <div className="btn" onClick={logout}>Выход</div>
            </div>}


              {this.props.user.role==="perfomer" && <div className="profile__menu">
              <ul className="profile__menu-list">
                <li className="profile__menu-item">
                  <a href="">Баланс:   1234 Р       <span className="user-popup__add">пополнить</span>  </a>
                </li>
                <li className="profile__menu-item">
                  <a href="">Тариф:     Стандарт    <span className="user-popup__add">изменить</span></a>
                </li>
                <li className="profile__menu-item">
                  <a href="">Диалоги     (+1)</a>
                </li>
                <li className="profile__menu-item">
                  <a href="">Моя техника   (1)</a>
                </li>
                <li className="profile__menu-item">
                  <a href="">отклики на заявки     (+1)</a>
                </li>
                <li className="profile__menu-item">
                  <a href="">Финансовые операции</a>
                </li>
                <li className="profile__menu-item">
                  <a href="">Отзывы (2/1)</a>
                </li>
                <li className="profile__menu-item">
                  <a href="" className="active">Мой профиль</a>
                </li>
                <li className="profile__menu-item">
                  <a href="">Настройки</a>
                </li>
              </ul>
              <div className="btn" onClick={logout}>Выход</div>
            </div>}
            <div className="profile__block">
              <div className="profile__title page-title">
                <span>МоЙ профиль</span>
              </div>
              <div className="profile__info">
                <div className="profile__info-item">Заявок на технику:<span className="profile__info-item-sum">1515</span></div>
                <div className="profile__info-item">Заявок на работу:<span className="profile__info-item-sum">151</span></div>
                <div className="profile__info-item">Партнеров:<span className="profile__info-item-sum">15</span></div>
              </div>
              <div className="profile__main">
                <div className="profile__main-info">
                  <div className="profile__main-info-block">
                    <AvatarField src={avatar_link ? avatar_link : Avatar} onChange={updateUser} />
                    <div className="profile__main-name-wrapper">
                      <div className="profile__name">
                        <span onClick={toggleStatusModal}>{this.props.user.name}</span>
                      </div>
                      <div className="profile__main-rate">
                        <span className="profile__rate-name">Рейтинг</span>
                        <span className="profile__rate-index">4.0</span>
                        <div className="profile__rate-wrapper"></div>
                        <div className="profile__rate-block"></div>
                      </div>
                    </div>
                  </div>
                  <div className="profile__main-input">
                    <ProfileField
                      label="ФИО:"
                      name="name"
                      icon={User}
                      value={name}
                      onSave={updateUser}
                    />
                    <ProfileCity
                      initialValues={{ address }}
                      onSave={updateUser}
                    />
                    <ProfileEmail
                      initialValues={{email}}
                      onSubmit={updateUser}
                    />
                    <ProfilePhone
                      initialValues={{phone}}
                      onSubmit={updateUser}
                    />
                    <ProfileDateField
                      label="Дата рожд:"
                      name="birth_date"
                      icon={Date}
                      value={birth_date ? birth_date : updated_at}
                      onSave={updateUser}
                    />
                    <RoleSwitcher role={role} onChange={updateUser} styles={roleSwitcherStyles} />
                    {/* <StatusField status={role} onChange={updateUser} /> */}
                    {!refered_by && <div onClick={toggleModal} className="btn btn-promo">ПРОМО</div>}
                  </div>

                </div>
                <div className="profile__main-stat">
                  <table>
                    <tbody>
                      <tr>
                        <td className="profile__stat-title">Заявки на технику</td>
                        <td className="profile__stat-title">Заявки на работу</td>
                        <td className="profile__stat-title">Партнеры</td>
                      </tr>
                      <tr>
                        <td>№ 123 (08.12.2018)</td>
                        <td>№12 (08.12.2018)</td>
                        <td>№1 (08.12.2018)</td>
                      </tr>
                      <tr>
                        <td>№ 123 (08.12.2018)</td>
                        <td>№12 (08.12.2018)</td>
                        <td>№1 (08.12.2018)</td>
                      </tr>
                      <tr>
                        <td>№ 123 (08.12.2018)</td>
                        <td>№12 (08.12.2018)</td>
                        <td>№1 (08.12.2018)</td>
                      </tr>
                      <tr>
                        <td>№ 123 (08.12.2018)</td>
                        <td>№12 (08.12.2018)</td>
                        <td>№1 (08.12.2018)</td>
                      </tr>
                      <tr>
                        <td>№ 123 (08.12.2018)</td>
                        <td>№12 (08.12.2018)</td>
                        <td>№1 (08.12.2018)</td>
                      </tr>
                      <tr>
                        <td>№ 123 (08.12.2018)</td>
                        <td>№12 (08.12.2018)</td>
                        <td>№1 (08.12.2018)</td>
                      </tr>
                      <tr>
                        <td>№ 123 (08.12.2018)</td>
                        <td>№12 (08.12.2018)</td>
                        <td>№1 (08.12.2018)</td>
                      </tr>
                      <tr>
                        <td>№ 123 (08.12.2018)</td>
                        <td>№12 (08.12.2018)</td>
                        <td>№1 (08.12.2018)</td>
                      </tr>
                    </tbody>
                  </table>
                </div>
              </div>
            </div>
          </div>
        </div>

        <ReactModal
          ariaHideApp={false}
          isOpen={promoVisible}
          contentLabel="Promo"
          onRequestClose={toggleModal}
          style={{
            overlay: {
              zIndex: 100
            },
            content: {
              color: '#111',
              position: 'absolute',
              height: '400px',
              width: '400px',
              top: '200px',
              margin: '0 auto',
              border: '1px solid #dbdbdb',
              background: '#f7f7f7',
              outline: 'none'
            }
          }}
        >
          <div className="promo__wrapper">
            <div className="promo__block">
              <h3>Введите промо-код</h3>
              <input type="text" onChange={promoChange} />
              <div className="btn" onClick={confirmPromo}>Подтвердить</div>
            </div>
            <div className="promo__close" onClick={toggleModal}><i className="fas fa-times"/></div>
          </div>
        </ReactModal>

        {/* <img src={this.props.user.picture} alt=""/>
        <div>{this.props.user.name}</div>
        <div>{this.props.user.nickname}</div>
        <div>{this.props.user.email}</div> */}

        {/* <EditField value={this.props.user.name}/>  */}
        <style jsx>{`
        .user-popup__add {
        color: #800;
        text-align: right;
        text-transform: lowercase;
        font-size: 18px;
        font-weight: 100;
      }
          .page-title {
             padding-top: 0;
             justify-content: flex-start;
             margin-left: 80px;
          }
          .profile__title.page-title span {
						font-size: 24px !important;

					}
          .page-title::before {
            top: 16px;
          }
          .page-title::after {
            top: 16px;
          }
          .profile__wrapper {
            display: flex;
            margin-bottom: 200px;
          }
          .profile__menu {
            margin-right: 20px;
            width: 250px;
          }
          .profile__menu-list {
            list-style: none;
            margin: 0;
            padding: 0;
            text-transform: uppercase;
            font-weight: 100;
            margin-bottom: 20px;
            background: #f7f7f7;
            font-size: 18px;
          }
          .profile__menu a {
            color: #747474;
            text-decoration: none;
            padding: 8px 5px 8px 20px;
            display: block;
          }
          .profile__menu-item a {
            display: flex;
            justify-content: space-between;
          }
          .profile__menu-item a:hover {
            color: #880000;
          }
          .profile__menu-item>.active {
            color: #880000;
          }
          .profile__block {
            flex-grow: 1;
          }
          .profile__info {
            display: flex;
            padding: 15px 40px;
            background: #f7f7f7;
            justify-content: space-between;
            margin-bottom: 50px;
          }
          .profile__info-item-sum {
            margin-left: 10px;
          }
          .profile__main {
            display: flex;
            background: #fff;
            padding: 20px;
          }
          .profile__main-info {
            width: 50%;
          }
          .profile__main-info-block {
            display: flex;
            margin-bottom: 40px;
          }
          .profile__name {
            font-size: 24px;
            font-weight: lighter;
            text-transform: uppercase;
            margin-bottom: 40px;
          }
          .profile__main-rate {
            font-size: 18px;
            font-weight: lighter;
            text-transform: uppercase;
            position: relative;
          }
          .profile__rate-wrapper {
            background: #880000;
            height: 40px;
            width: 160px;
            transform: skewX(-30deg);
          }
          .profile__rate-block {
            background: #fff;
            height: 30px;
            width: 160px;
            transform: skewX(-30deg);
            position: relative;
            top: -35px;
            left: 60px;
            -webkit-box-shadow: 0px 0px 10px 2px rgba(163,160,163,1);
            -moz-box-shadow: 0px 0px 10px 2px rgba(163,160,163,1);
            box-shadow: 0px 0px 10px 2px rgba(163,160,163,1);
          }
          .profile__rate-index {
            color: #fff;
            font-size: 16px;
            position: absolute;
            top: 45px;
            left: 19px;
            z-index: 2;

          }
          .profile__rate-name {
            margin-bottom: 10px;
            display: block;
          }
          .profile__main-input {
            display: flex;
            flex-direction: column;
          }
          .profile__main-input-item {
            font-size: 18px;
            padding: 10px;
            border-bottom: 1px solid #d8d8d8;
            width: 350px;
            display: flex;
            justify-content: space-between;
            align-items: center;
            position: relative;
          }
          .profile__input-img {
            position: absolute;
            left: 7px;
            top: 13px;
          }
          .profile__input-block {
            margin-right: 70px;
          }
          .profile__input-descr {
            margin-right: 10px;
            font-size: 16px;
            font-weight: lighter;
          }
          .profile__input-editable {
            font-size: 18px;
            text-transform: uppercase;
          }
          .profile__main-stat {
            width: 50%;
          }
          .profile__main-stat table {
            width: 100%;
          }
          .profile__stat-title {
            color: #880000;
          }

          td {
            border: 1px solid #d8d8d8;
            padding: 10px;
            font-weight: lighter;
            text-align: center;
          }
          .btn.btn-promo {
            width: 350px
          }

          .promo__wrapper {
            position: relative;
          }
          .promo__block {
            display: flex;
            justify-content: center;
            flex-direction: column;
            padding-top: 30px;
          }
          .promo__block h3 {
            margin-bottom: 30px;
            text-align: center;
          }
          .promo__block input {
            border: 1px solid #dbdbdb;
            height: 40px;
            margin-bottom: 50px;
            outline: none;
          }

          .promo__close {
            position: absolute;
            top: -5px;
              right: -5px;
              font-size: 30px;
              cursor: pointer;
              -webkit-transition: -webkit-transform 0.3s;
              -webkit-transition: transform 0.3s;
              transition: transform 0.3s;
              line-height: 0;
          }
          .promo__close:hover {
            transform: rotate(180deg);
          }



          @media (max-width: 1440px) {
            	.container {
								width: 1200px;
							}
              .profile__img {
             width: 150px;
            }
            .profile__main-info-block {
              margin-bottom: 0;
            }
          }
						@media (max-width: 1280px) {
							.container {
								width: 1088px;
							}

              .profile__main-rate {
                  top: 100px;
                  left: -167px;
              }
              .profile__main-info-block {
                  margin-bottom: 90px;
              }
              .profile__info {
                margin-bottom: 30px;
              }
            }
						@media (max-width: 1150px) {
							.container {
								width: 990px;
							}
              .profile__main {
                flex-direction: column;
              }
              .profile__main-info {
                display: flex;
                width: 100%;
                margin-bottom: 40px;
              }
              .profile__main-stat {
                width: 100%;
              }
              .profile__menu-list {
                font-size: 16px;
              }
						}
						@media (max-width: 1024px) {
							.container {
								width: auto;
								padding: 0 15px;
							}
              .profile__menu {
                min-width: 190px;
              }
              .profile__menu-list {
                font-size: 14px;
              }
						}




				`}</style>
        </div>
    )
  }
}
