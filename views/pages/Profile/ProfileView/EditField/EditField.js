
import React, { Component } from 'react'
import { connect } from "react-redux";
import { Field, reduxForm } from 'redux-form'

class EditField extends Component {
  constructor(props) {
    super(props)
    this.state = {
      edit: false,
      inputValue: props.value
    }
  }
  // const renderField = (field) => (
  //   <div className="input-row">
  //     <input {...field.input} type="text"/>
  //     {field.meta.touched && field.meta.error &&
  //      <span className="error">{field.meta.error}</span>}
  //   </div>
  // )
  onChange= (e) => {
    console.log(e);
    this.setState({inputValue: e.target.value})}
  render() {
    return (
      <div>

         {this.state.edit && <input name="firstName"  type="text" onChange={this.onChange} value={this.state.inputValue}/>}
         {!this.state.edit && <span>{this.props.value}</span>}
        <div onClick={()=> this.setState({edit: !this.state.edit})}>edit</div>
      </div>
    )
  }
}

EditField = reduxForm({
  // a unique name for the form
  form: 'contact'
})(EditField)

export default EditField