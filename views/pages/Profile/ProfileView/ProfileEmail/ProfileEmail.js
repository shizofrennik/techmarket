import React, { Component } from 'react';
import { Field, reduxForm } from 'redux-form';
import Edit from '../img/input-edit.png';
import Date from '../img/input-date.png';
import Email from '../img/input-email.png';
import { validation } from '../../../../../helpers/utils'

class ProfileEmail extends Component {
  state = {
    active: false
  }

  edit = () => {
    this.setState({ active: true });
  }

  save = async values => {
    const { onSubmit } = this.props;

    try {
      await onSubmit(values);
      this.setState({ active: false });
    } catch(error) {
      console.log(error);
    }
  }

  render() {
    const { edit, save } = this;
    const { handleSubmit, invalid, submitting } = this.props;
    const { active } = this.state;

    return (
      <form className="profile__main-input-item" onSubmit={handleSubmit(save)}>
        <img src={Email} alt="" className="profile__input-img"/>
        <div className="profile__input-block">
          <span className="profile__input-descr">Email:</span>
          <Field
            className="profile__input-editable"
            component="input"
            name="email"
            disabled={!active}
            validate={validation.email}
          />
        </div>
        {active ? (
          <button type="submit" disabled={invalid || submitting}><img src={Date} alt="" /></button>
        ) : (
          <div onClick={edit}><img src={Edit} alt="" /></div>
        )}
      </form>
    )
  }
}

const ProfileEmailForm = reduxForm({
  form: 'profileEmail',
  enableReinitialize: true
})(ProfileEmail)

export default ProfileEmailForm;