import React, { Component } from 'react';
import Link from 'next/link'

class HomeView extends Component {
  render() {
    return (
      <div>
        Home page
        <Link href="/other">
          <a>other page</a>
        </Link>

        <style global jsx>{`
        `}</style>
      </div>
    );
  }
}

export default HomeView;