import React, { Component } from 'react';
import Link from 'next/link'
import vk from './img/vk.png';
import fb from './img/fb.png';
import ok from './img/ok.png';
// import SignInForm from './SignInForm'
import PhoneForm from './PhoneForm'
import { Field, reduxForm } from 'redux-form'
import { isAbsolute } from 'path';

class SignInView extends Component {
  state = {
    agreement: true
  }

  handleSubmit = () => {
    this.props.getApiAccessToken('eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiIsImtpZCI6Ik9FVXhNamMzTlRoRE56WTROVFZCTTBGRU0wUkZORGcwTlVJME5VTXhOekJHUkRkQk5qUkdSQSJ9.eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiIsImtpZCI6Ik9FVXhNamMzTlRoRE56WTROVFZCTTBGRU0wUkZORGcwTlVJME5VTXhOekJHUkRkQk5qUkdSQSJ9.eyJpc3MiOiJodHRwczovL3RlaGNsdWIuYXV0aDAuY29tLyIsInN1YiI6InNtc3w1YmZlYzRlOTY1YWY5OGVmODI2Mjk3OTYiLCJhdWQiOlsiaHR0cDovL3huLS05MGFocGU2YWpsLnhuLS1wMWFpL2FwaSIsImh0dHBzOi8vdGVoY2x1Yi5hdXRoMC5jb20vdXNlcmluZm8iXSwiaWF0IjoxNTQzNTA4NDMwLCJleHAiOjE1NDM1MTU2MzAsImF6cCI6Im1HRXM3enpGMVZNS092YUxBSjJBbExESFBoSzNyS2FDIiwic2NvcGUiOiJvcGVuaWQgcHJvZmlsZSBlbWFpbCJ9.IM13vKgHJhDSWQ0JiYBBmcnxhWvC9GD73zDZASvdtx_ga7S40zxfwI8I7jxbp744cA3Y653EYLzVOPWcTBhPytxBG03ysf388XCs3Kg3Wv3RAboZ_-nmje9z3YVX0tQBBhBXkHT5xwO5V84X0oRnNLjy5MGnve89hgcqW8s3Be3Wi2cJPJ6ItnBZYz_wFGKCN_Sd0KcofY_RCvFttrCvq3O93Y2Qzs_Ot7kxyFOM3g660Sf8ztimeF-ROHmYkrx6QcF9WRIY_T6Gv3Rqhg-BMrnLpViAMrx2KrTVYKbWqMMJnyTkPLyIOiDIAo-VsotV_pNp4FSkM5G7uzi4Gv53yg');
    // this.props.signUp({email: 'tech@test.com', password: '123456', username: 'techmarket'});
    // this.props.login({username: 'tech@test.com', password: '123456'});
  }

  toggleAgreement = () => {
    this.setState(({ agreement }) => ({ agreement: !agreement }))
  }

  render() {
    const { toggleAgreement } = this;
    const { loginSocial, smsLogin, smsVerify } = this.props;
    const { agreement } = this.state;

    return (
      <div>
        <div className="auth">
          <div className="container">
            <div className="auth__wrapper">
              <div className="auth__title page-title">
                <span onClick={this.handleSubmit}>регистрация</span>
              </div>
              <div className="auth__block">
                <div className="auth__form">

                    <span>Через аккаунт в соц.сети</span>
                    <button className="btn" disabled={!agreement} onClick={() => loginSocial('vkontakte')}><img src={vk} alt=""/>vkontakte</button>
                    <button className="btn" disabled={!agreement} onClick={() => loginSocial('facebook')}><img src={fb} alt=""/>facebook</button>
                    <button className="btn" disabled={!agreement} onClick={() => loginSocial('google-oauth2')}><img src={ok} alt=""/>google</button>
                    <button className="btn" disabled={!agreement}><img src={ok} alt=""/>instagram</button>
                    <span>или заполните форму</span>
                  <PhoneForm smsLogin={smsLogin} smsVerify={smsVerify} agreement={agreement} />
                  <label className="checklabel">
                    <input type="checkbox" checked={agreement} value={agreement} onChange={toggleAgreement}/>
                    <span className="checkmark"></span> Согласен с <Link href='/terms'><a>Правилами сервиса</a></Link>
                  </label>
                </div>
                <div className="auth__descr">
                  <div className="auth__descr-title">Что дает регистрация</div>
                  <div className="auth__descr-block">
                    <div className="auth__descr-name">Для Заказчиков</div>
                    <ul className="auth__descr-list">
                      <li className="auth__descr-item">В каталоге техники Вам станут доступны контакты владельцев</li>
                      <li className="auth__descr-item">Беплатное размещение заявки на необходимую технику</li>
                      <li className="auth__descr-item">Мгновенное опвещение всех владельцев техники о Вашей заявки</li>
                      <li className="auth__descr-item">Бесплатное размещение заявки на выполнение любого вида работ </li>
                      <li className="auth__descr-item">Общение в клубном онлайн чате</li>
                    </ul>
                  </div>
                  <div className="auth__descr-block">
                    <div className="auth__descr-name">Для Исполнителей</div>
                    <ul className="auth__descr-list">
                      <li className="auth__descr-item">Размещение своей ехники в Каталоге клуба без ограничению по количеству прадложений и сроку размещения</li>
                      <li className="auth__descr-item">Мгновенное получение уведомление о новых заявках на технику после их публикации</li>
                      <li className="auth__descr-item">Получение уведомление о новых заявки на выполнения работ</li>
                      <li className="auth__descr-item">Общение в клубном онлайн чате</li>
                    </ul>
                  </div>
                  <div className="auth__descr-block">
                    <div className="auth__descr-name">Для Всех</div>
                    <ul className="auth__descr-list">
                      <li className="auth__descr-item">Возможность зарабатывать по Партнерской программе</li>
                    </ul>
                  </div>
                </div>
              </div>
            </div>
          </div>

        </div>
        <style jsx>{`
          .container {
            width: 1368px;
            max-width: 1368px;
            padding-right: 0;
            padding-left: 0;
          }
          .auth {
            background: #f2f2f2;
          }
          .auth__wrapper {
            padding-bottom: 100px;
          }
          .auth__block {
            display: flex;
            justify-content: space-between;
          }
          .auth__form {
            width: 300px;
            margin-left: 100px;
          }
          .auth__form form {
            display: flex;
            flex-direction: column;
            justify-content: center;
            align-items: center;
          }
          .auth__form form>span {
            margin-bottom: 15px;
            text-transform: uppercase;
            font-size: 18px;
            font-weight: 100;
          }
          .auth__form input[type="checkbox"] {
             display: none;
           }
          .auth__form input[type="text"] {
            width: 100%;
            border: 1px solid #565656;
            margin-bottom: 20px;
            height: 50px;
            padding: 5px 10px 5px 20px;
            outline: none;
            font-size: 16px;
            font-weight: 100;
            color: #565656;
            background: #f2f2f2;
          }
          .checklabel {
            position: relative;
            margin-left: 65px;
          }
          .checklabel a {
            color: #111111;
            border-bottom: 1px solid #888888;
            text-decoration: none;
          }
          .auth__form-agreement input {
            margin-right: 5px;
          }
          .auth__form .checkmark {
            display: block;
            height: 14px;
            width: 14px;
            border: 1px solid #565656;
            position: absolute;
            top: 5px;
            left: -25px;
          }
          .auth__form .checkmark::after {
            content: '';
            display: none;
            width: 8px;
            height: 8px;
            background: #880000;
            position: absolute;
            top: 2px;
            left: 2px;
          }
          .auth__form input[type="checkbox"]:checked ~ .checkmark:after {
             display: block;
          }
          .auth__form .btn {
            width: 100%;
          }
          .auth__form .btn img {
            margin-right: 20px;
          }
          .auth__descr {
            width: 40%;
          }
          .auth__descr-title {
            font-size: 18px;
            margin-bottom: 20px;
            text-transform: uppercase;
            display: flex;
            justify-content: center;
          }
          .auth__descr-name {
            font-size: 18px;
            font-weight: 100;
            margin-bottom: 15px;
          }
          .auth__descr-list {
            font-weight: 100;
            padding: 0;
            padding-left: 20px;
            list-style: none;
          }
          .auth__descr-item {

          }
          .auth__descr-item::before {
            content: '';
            display: block;
            height: 4px;
            width: 4px;
            background: #880000;
            position: relative;
            top: 13px;
            left: -20px;
          }


			@media (max-width: 1440px) {
							.container {
								width: 1200px;
							}
						}
						@media (max-width: 1280px) {
							.container {
								width: 1088px;
							}
						}
						@media (max-width: 1150px) {
							.container {
								width: 990px;
							}
              .auth__descr {
                width: 50%;
              }
						}
						@media (max-width: 1024px) {
							.container {
								width: auto;
								padding: 0 15px;
							}
              .auth__form {
                margin-left: 10px;
              }
              .auth__descr {
                width: calc(100% - 390px);
              }
						}


        `}</style>
      </div>
    );
  }
}



export default SignInView;