import React, { Component } from 'react'
import { connect } from "react-redux";
import { Field, reduxForm } from 'redux-form'

class SignInForm extends Component {

  render() {
    return (
      <div>
        <form onSubmit={this.props.handSubmit}>
          <div>
        
            <Field name="firstName" component="input" type="text" placeholder="Имя"/>
          </div>
          <div>
       
            <Field name="lastName" component="input" type="text" placeholder="+7 ( ХХХ ) ХХ ХХ ХХ" />
          </div>
          
          <div type="submit">Submit</div>
          <div>
         
          <Field
            name="employed"
            id="employed"
            component="input"
            type="checkbox"
          /> <label htmlFor="employed">Employed</label>
        </div>
        </form>
      </div>
     
    )
  }
}




SignInForm = reduxForm({
  // a unique name for the form
  form: 'contact'
})(SignInForm)

export default SignInForm