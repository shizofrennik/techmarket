import React, { Component } from 'react';
import {
  Field,
  reduxForm
} from 'redux-form';
import { InputWithError } from '../../../../components/form-fields';
import PhoneConfirmForm from './PhoneConfirmForm';
import {
  required,
  validation,
  phoneMask
} from '../../../../../helpers/utils';

class PhoneForm extends Component {
  state = {
    isModalOpen: false,
    phoneNumber: ''
  }

  onSubmit = ({ phoneNumber }) => {
    const { smsLogin } = this.props;
    phoneNumber = `+7${phoneNumber}`;

    this.setState(
      {
        isModalOpen: true,
        phoneNumber
      },
      () => {
        smsLogin(phoneNumber);
      }
    )
  }

  onCloseModal = () => {
    this.setState({ isModalOpen: false })
  }

  render() {
    const {
      onSubmit,
      onCloseModal
    } = this;
    const {
      handleSubmit,
      invalid,
      submitting,
      agreement,
      smsVerify,
      smsLogin
    } = this.props;
    const {
      isModalOpen,
      phoneNumber
    } = this.state;

    return (
      <React.Fragment>
        <form className="auth__form" onSubmit={handleSubmit(onSubmit)} noValidate>
          <Field
            type="tel"
            autoComplete="new-phoneNumber"
            name="phoneNumber"
            component={InputWithError}
            validate={[validation.required, validation.phone]}
            {...phoneMask}
          />
          <button className="btn" name="login" type="submit" disabled={invalid || submitting || !agreement}>ВОЙТИ</button>
          <style jsx>{`
            .auth__form .btn {
              width: 100%;
            }
          `}</style>
        </form>

        {isModalOpen &&
        <PhoneConfirmForm
          onCloseModal={onCloseModal}
          smsVerify={smsVerify}
          smsLogin={smsLogin}
          phoneNumber={phoneNumber}
        />}
      </React.Fragment>
    )
  }
}

export default reduxForm({
  form: 'phoneSubmit'
})(PhoneForm);