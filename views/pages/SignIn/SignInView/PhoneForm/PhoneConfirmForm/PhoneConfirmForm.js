import React, { Component } from 'react';
import { Field, reduxForm } from 'redux-form';
import ReactModal from 'react-modal';
import { InputWithError } from '../../../../../components/form-fields';
import { validation } from '../../../../../../helpers/utils';

class PhoneConfirmForm extends Component {
  state = {
    canReSend: false,
    sec: 60
  }

  componentDidMount() {
    this.interval = setInterval(this.tick, 1000);
  }

  componentWillUnmount() {
    clearInterval(this.interval);
  }

  onSubmit = ({ verificationCode }) => {
    const { smsVerify, phoneNumber } = this.props;

    try {
      smsVerify({phoneNumber, verificationCode});
    } catch(error) {
      console.log(error)
    }
  }

  reSend = () => {
    const { smsLogin, phoneNumber } = this.props;
    this.setState(
      {
        canReSend: false
      },
      () => {
        this.interval = setInterval(this.tick, 1000);
        try {
          smsLogin(phoneNumber);
        } catch(error) {
          console.log(error)
        }
      }
    )
  }

  tick = () => {
    const { sec } = this.state;

    if (sec === 1) {
      this.setState(
        {
          sec: 60,
          canReSend: true
        },
        () => {
          clearInterval(this.interval)
        }
      )
    } else {
      this.setState({ sec: sec - 1 })
    }
  }

  render() {
    const {
      onSubmit,
      reSend
    } = this;
    const {
      isOpen,
      onCloseModal,
      phoneNumber,
      handleSubmit,
      invalid,
      submitting
    } = this.props;
    const {
      canReSend,
      sec
    } = this.state;

    return (
      <ReactModal
        ariaHideApp={false}
        isOpen={true}
        contentLabel="Подтвердите номер"
        onRequestClose={onCloseModal}
        style={{
          overlay: {
            zIndex: 100
          },
          content: {
            color: '#111',
            width: '700px',
            margin: '0 auto',
            border: '1px solid #dbdbdb',
            background: '#f7f7f7',
            outline: 'none',
            bottom: 'unset'
          }
        }}
      >
        <form className="modal__wrapper" onSubmit={handleSubmit(onSubmit)}>
          <div className="modal__block">
            <h3>Подтвердите номер</h3>
            <h6>На указанный номер телефона {phoneNumber} отправлено SMS с кодом, введите его.</h6>
            <div className="modal-phoneconfirm__row">
              <div className="modal-phoneconfirm__row_item">
                <Field
                  type="text"
                  placeholder="Проверочный код"
                  name="verificationCode"
                  component={InputWithError}
                  validate={[validation.required, validation.verificationCode]}
                />
              </div>
              <div className="modal-phoneconfirm__row_item">
                <button className="btn" name="request" type="submit" disabled={invalid || submitting}>ПОДТВЕРДИТЬ</button>
              </div>
            </div>
            <div className="modal-phoneconfirm__row">
              {canReSend ? (
                <button className="btn" name="request" type="button" onClick={reSend}>ВЫСЛАТЬ КОД ПОВТОРНО</button>
              ) : (
                <div>Не пришло СМС с кодом? Отправить код повторно можно через <span style={{color: '#880000'}}>{sec} сек</span></div>
              )}
            </div>
          </div>
          <div className="modal__close" onClick={onCloseModal}><i className="fas fa-times"/></div>
        </form>
      </ReactModal>
    )
  }
}

export default reduxForm({
  form: 'phoneConfirmForm'
})(PhoneConfirmForm);