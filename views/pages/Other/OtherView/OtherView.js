import React, { Component } from 'react';
import Link from 'next/link'

class OtherView extends Component {
  render() {
    return (
      <div>
        Other page
        <Link href="/">
          <a>home page</a>
        </Link>
      </div>
    );
  }
}

export default OtherView;