import React, { Component } from 'react';
import ModalConfirm from './ModalConfirm';

const roles = {
  customer: 'Заказчик',
  perfomer: 'Исполнитель'
}

class RoleSwitcher extends Component {
  state = {
    role: this.props.role,
    prevRole: ''
  }

  componentWillReceiveProps(props) {
    const { role } = props;
    if(role !== this.state.role) this.setState({role});
  }

  onConfirm = async () => {
    const { onChange } = this.props;
    const { role } = this.state;

    try {
      await onChange({ role });
      this.setState({ role });
      this.toggleModal()
    } catch(error) {
      console.log(error);
    }
  }

  onCancel = () => {
    const { prevRole } = this.state;

    this.setState({ role: prevRole }, () => {
      this.toggleModal();
    });
  }

  toggleModal = () => this.setState(({ isModalVisible }) => ({ isModalVisible: !isModalVisible }));

  onChange = e => {
    e.preventDefault();
    const { role } = this.state;
    const newRole = e.target.value;

    this.setState(
      {
        role: newRole,
        prevRole: role
      },
      () => {
        this.toggleModal();
      }
    );
  }

  render() {
    const { onChange, onConfirm, onCancel } = this;
    const { styles } = this.props;
    const { isModalVisible, role } = this.state;

    return (
      <React.Fragment>

        <div className="profile-role" style={styles.wrapper}>
          <label className="profile-role-item" style={role === 'customer' ? {...styles.item, ...styles.activeItem} : styles.item}>
            <input
              checked={role === 'customer'}
              type="radio"
              name="role"
              value="customer"
              className="btn btn-radio"
              onChange={onChange}
            />
            {roles.customer}
          </label>
          <label className="profile-role-item" style={role === 'perfomer' ? {...styles.item, ...styles.activeItem} : styles.item}>
            <input
              checked={role === 'perfomer'}
              type="radio"
              name="role"
              value="perfomer"
              className="btn btn-radio"
              onChange={onChange}
            />
            {roles.perfomer}
          </label>
        </div>

        {isModalVisible &&
        <ModalConfirm
          isOpen={isModalVisible}
          onClose={onCancel}
          onConfirm={onConfirm}
          status={role}
        >
          Вы действительно хотите изменить статус на "{roles[role]}"?
        </ModalConfirm>}

        <style jsx>{`
          .profile-role-item input {
            display: none;
          }
        `}</style>

      </React.Fragment>
    )
  }
}

export default RoleSwitcher;