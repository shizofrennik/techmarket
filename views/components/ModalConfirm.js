import React, { Component } from 'react';
import ReactModal from 'react-modal';

const styles = {
  overlay: {
    zIndex: 100
  },
  content: {
    color: '#111',
    position: 'absolute',
    height: '400px',
    width: '400px',
    top: '200px',
    margin: '0 auto',
    border: '1px solid #dbdbdb',
    background: '#f7f7f7',
    outline: 'none'
  }
};

class ModalConfirm extends Component {
  render() {
    const {
      isOpen,
      onClose,
      onConfirm,
      children
    } = this.props;

    return (
      <React.Fragment>
        <ReactModal
          ariaHideApp={false}
          isOpen={isOpen}
          contentLabel="Status"
          onRequestClose={onClose}
          style={styles}
        >
          <div className="modal__wrapper">
            <div className="modal__block">
              <h3>{children}</h3>
              <div className="btn" onClick={onConfirm}>Подтвердить</div>
            </div>
            <div className="modal__close" onClick={onClose}><i className="fas fa-times"/></div>
          </div>
        </ReactModal>
        <style jsx>{`
          .modal__wrapper {
            position: relative;
          }
          .modal__block {
            display: flex;
            justify-content: center;
              flex-direction: column;
              padding-top: 30px;
          }
          .modal__block h3 {
            margin-bottom: 30px;
            text-align: center;
          }
          .modal__block input {
            border: 1px solid #dbdbdb;
            height: 40px;
            margin-bottom: 50px;
          }

          .modal__close {
            position: absolute;
            top: -5px;
              right: -5px;
              font-size: 30px;
              cursor: pointer;
              -webkit-transition: -webkit-transform 0.3s;
              -webkit-transition: transform 0.3s;
              transition: transform 0.3s;
              line-height: 0;
          }
          .modal__close:hover {
            transform: rotate(180deg);
          }
        `}</style>
      </React.Fragment>
    )
  }
}

export default ModalConfirm;