import React from 'react';
import { Select } from 'antd';
import cities from '../../helpers/cities.json';
// console.log(cities)

export const InputWithError = ({ input, type, placeholder, meta: { error, touched, invalid } }) => (
  <div>
    <input type={type} placeholder={placeholder} {...input} />
    {invalid && touched && <span className="error">{error}</span>}
    <style jsx>{`
      input {
        width: 100%;
        border: 1px solid #565656;
        margin-bottom: 20px;
        height: 50px;
        padding: 5px 10px 5px 20px;
        outline: none;
        font-size: 16px;
        font-weight: 100;
        color: #565656;
        background: #f2f2f2;
      }
      .error {
        display: block;
        color: red;
        margin-top: -15px;
      }
    `}</style>
  </div>
)

const Option = Select.Option;

export const SelectInput = ({ input: { onFocus, onBlur, onChange, value }, disabled, options, placeholder }) => (
  <Select
    showSearch
    placeholder={placeholder}
    optionFilterProp="children"
    onChange={onChange}
    onFocus={onFocus}
    value={value}
    onBlur={onBlur}
    disabled={disabled}
    filterOption={(input, option) => option.props.children.toLowerCase().indexOf(input.toLowerCase()) >= 0}
  >
    {options.map((option, index) => <Option key={index} value={option}>{option}</Option>)}
  </Select>
)