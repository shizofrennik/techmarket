import React from 'react'
import {connect} from 'react-redux'
import OtherView from '../views/pages/Other';
import Layout from '../views/layouts';
import {syncUser} from '../redux/actions/auth';

class Other extends React.Component {
  static async getInitialProps ({ctx: {store, isServer, req }}) {
    if(isServer) store.dispatch(syncUser({req, isServer}));
  }

  render () {
    return (
      <div>
        <Layout>
          <OtherView />
        </Layout>
      </div>
    )
  }
}

export default connect()(Other)
