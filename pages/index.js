import React from 'react'
import {connect} from 'react-redux'
import HomeView from '../views/pages/Home';
import Layout from '../views/layouts';
import { syncUser } from '../redux/actions/auth';

class Index extends React.Component {
  static async getInitialProps ({ctx: {store, isServer, req }}) {
    if(isServer) store.dispatch(syncUser({req, isServer}));
  }

  render () {
    return (<div>
      <Layout>
        <HomeView />
      </Layout>
    </div>)
  }
}

export default connect()(Index)
