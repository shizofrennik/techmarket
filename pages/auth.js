import React from 'react'
import {connect} from 'react-redux'
import {bindActionCreators} from 'redux';
import {parseHash} from '../redux/actions/auth';

class Auth extends React.Component {
  componentWillMount() {
    this.props.parseHash();
  }

  render() {
    return (
      <div className="container" style={{display: "flex", justifyContent: "center", alignItems: "center", height: "70vh"}}>
        Loading...
      </div>
    );
  }
}

const mapDispatchToProps = (dispatch) => {
  return bindActionCreators({
    parseHash
  }, dispatch);
}

export default connect(null, mapDispatchToProps)(Auth)
