import React from 'react'
import {connect} from 'react-redux'
import {bindActionCreators} from 'redux';
import SignInView from '../views/pages/SignIn';
import Layout from '../views/layouts';
import {signUp, login, logout, syncUser, smsLogin, smsVerify, getApiAccessToken, loginSocial} from '../redux/actions/auth';

class SignIn extends React.Component {
  static async getInitialProps ({ctx: {store, isServer, req }}) {
    if(isServer) store.dispatch(syncUser({req, isServer}));
  }

  render () {
    const { signUp, login, logout, smsLogin, smsVerify, getApiAccessToken, loginSocial } = this.props;
    return (
      <Layout>
        <SignInView loginSocial={loginSocial} signUp={signUp} smsLogin={smsLogin} getApiAccessToken={getApiAccessToken} smsVerify={smsVerify} login={login} logout={logout} />
      </Layout>
    )
  }
}

const mapStateToProps = (state, props) => {
  return ({})
}

const mapDispatchToProps = (dispatch) => {
  return bindActionCreators({
    signUp,
    login,
    loginSocial,
    smsLogin,
    logout,
    smsVerify,
    getApiAccessToken
  }, dispatch);
}


export default connect(mapStateToProps, mapDispatchToProps)(SignIn)
