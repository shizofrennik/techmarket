import React from 'react'
import {connect} from 'react-redux'
import {bindActionCreators} from 'redux';
import ProfileView from '../views/pages/Profile';
import Layout from '../views/layouts';
import { syncUser, updateUser, logout } from '../redux/actions/auth';

class Profile extends React.Component {
  static async getInitialProps ({ctx: {store, isServer, req }}) {
    if(isServer) store.dispatch(syncUser({req, isServer}));
  }

  render () {

    const { user, updateUser, logout } = this.props;
    return (
      <Layout>
        <ProfileView user={user} updateUser={updateUser} logout={logout} />
      </Layout>
    )
  }
}

const mapStateToProps = (state, props) => {
  return ({
    user: state.auth.user,
  })
}

const mapDispatchToProps = (dispatch) => {
  return bindActionCreators({
    updateUser,
    logout
  }, dispatch);
}


export default connect(mapStateToProps, mapDispatchToProps)(Profile)
