import { createTextMask } from 'redux-form-input-masks';

const length = length => value => value.length !== length ? `Должно быть ${length} символа` : undefined;

export const validation = {
  required: value => !value ? 'Поле обязательно' : undefined,
  phone: value => /^\d{10}$/g.test(value) ? undefined : 'Введите номер в +7XXXXXXXXXX формате',
  verificationCode: value => /^\d{4}$/g.test(value) ? undefined : 'Введите 4 цифры',
  digits: value => /^\d+$/g.test(value) ? undefined : 'Должны быть только цифры',
  email: value => value && /^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i.test(value) ? undefined : 'Неверный адрес почты',
  length4: length(4)
}

export const phoneMask = createTextMask({
  pattern: '+7 (999) 999 99 99',
  guide: false
});

export const cities = ['Абакан','Альметьевск','Ангарск','Арзамас','Армавир','Артем','Архангельск','Астрахань','Ачинск','Балаково','Балашиха','Барнаул','Батайск','Белгород','Бердск','Березники','Бийск','Благовещенск','Братск','Брянск','Великий Новгород','Владивосток','Владикавказ','Владимир','Волгоград','Волгодонск','Волжский','Вологда','Воронеж','Грозный','Дербент','Дзержинск','Димитровград','Долгопрудный','Домодедово','Евпатория','Екатеринбург','Елец','Ессентуки','Железногорск','Жуковский','Златоуст','Иваново','Ижевск','Иркутск','Йошкар-Ола','Казань','Калининград','Калуга','Каменск - Уральский','Камышин','Каспийск','Кемерово','Керчь','Киров','Кисловодск','Ковров','Коломна', 'Комсомольск-на-Амуре','Копейск','Королёв','Кострома','Красногорск','Краснодар','Красноярск','Курган','Курск','Кызыл','Липецк','Люберцы','Магнитогорск','Майкоп','Махачкала','Миасс','Москва','Мурманск','Муром','Мытищи','Набережные Челны','Назрань','Нальчик','Находка','Невинномысск','Нефтекамск','Нефтеюганск','Нижневартовск','Нижнекамск','Нижний Новгород','Нижний Тагил','Новокузнецк','Новокуйбышевск','Новомосковск','Новороссийск','Новосибирск','Новочебоксарск','Новочеркасск','Новошахтинск','Новый Уренгой','Ногинск','Норильск','Ноябрьск','Обнинск','Одинцово','Октябрьский','Омск','Орёл','Оренбург','Орехово-Зуево','Орск','Пенза','Первоуральск','Пермь','Петрозаводск','Петропавловск-Камчатский','Подольск','Прокопьевск','Псков','Пушкино','Пятигорск','Раменское','Ростов-на-Дону','Рубцовск','Рыбинск','Рязань','Салават','Самара','Санкт-Петербург','Саранск ','Саратов','Севастополь','Северодвинск','Северск','Сергиев Посад','Серпухов','Симферополь','Смоленск', 'Сочи','Ставрополь','Старый Оскол','Стерлитамак','Сургут','Сызрань','Сыктывкар','Таганрог','Тамбов','Тверь','Тольятти','Томск','Тула','Тюмень','Улан - Удэ','Ульяновск','Уссурийск','Уфа','Хабаровск','Хасавюрт','Химки','Чебоксары','Челябинск','Череповец','Черкесск','Чита','Шахты','Щёлково','Электросталь','Элиста','Энгельс','Южно-Сахалинск','Якутск','Ярославль'];